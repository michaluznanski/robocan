/*
 * Message.hpp
 *
 *  Created on: Jun 7, 2021
 *      Author: Dell
 */

#ifndef INC_MESSAGE_HPP_
#define INC_MESSAGE_HPP_

#include <bitset>
#include <vector>
#include <cmath>

namespace RoboCAN {


// Wielkosci te jeszcze do poprawy, no i pytanie, czy nalezy te klase robic w ten sposob, czy w jakis inny
constexpr unsigned MASTER_ID_SIZE = 6U;
constexpr unsigned SLAVE_ID_SIZE = 10U;
constexpr unsigned FUNCTION_NUMBER_SIZE = 5U;
// dalej sa jeszcze wolne bity, moze potem sie przydadza, np. na numer ramki przy segmentacji danych
constexpr unsigned MASTER_ID_POS = (32U - MASTER_ID_SIZE);
constexpr unsigned SLAVE_ID_POS = (32U - MASTER_ID_SIZE - SLAVE_ID_SIZE);
constexpr unsigned FUNCTION_NUMBER_POS = (32U - MASTER_ID_SIZE - SLAVE_ID_SIZE - FUNCTION_NUMBER_SIZE);

class Message {
public:
	Message(unsigned masterId, unsigned slaveId, unsigned functionId, unsigned isMasterMessage = false, uint8_t * data = nullptr, unsigned DLC = 0);
	Message(const std::bitset<32>& msgId,
//			unsigned isMasterMessage = false,
			uint8_t * data = nullptr,
			unsigned DLC = 0U);


	unsigned getMasterId() const; //zamiast bitsetow mozna dac uinty
	unsigned getSlaveId() const;
	unsigned getFunctionNumber() const;
	unsigned getMessageId() const;
	const std::vector<uint8_t>& getData() const;


//	std::bitset<MASTER_ID_SIZE>& getMasterId(std::bitset<MASTER_ID_SIZE>& masterId); //zamiast bitsetow mozna dac uinty
//	std::bitset<SLAVE_ID_SIZE>& getSlaveId(std::bitset<SLAVE_ID_SIZE>& slaveId);
//	std::bitset<FUNCTION_NUMBER_SIZE>& getFunctionNumber(std::bitset<FUNCTION_NUMBER_SIZE>& functionNumber);
//	std::vector<uint8_t>& getData(std::vector<uint8_t>& data);

private:
	// czy moze lepiej zrobic oddzielna klase na sam naglowek
	std::bitset<32> messageId; //32 bo odwzorowuje caly rejestr id w kontrolerze CAN, choc mozna zmienic na 29
//	uint8_t dataLength; zapamietujemy to w wektorze
	std::vector<uint8_t> data;
};

} /* namespace RoboCAN */

#endif /* INC_MESSAGE_HPP_ */
