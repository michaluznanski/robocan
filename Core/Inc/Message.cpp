/*
 * Message.cpp
 *
 *  Created on: Jun 7, 2021
 *      Author: Dell
 */

#include <Message.hpp>

namespace RoboCAN {

Message::Message(unsigned masterId, unsigned slaveId, unsigned functionId, unsigned isMasterMessage, uint8_t * data, unsigned DLC):
		messageId(masterId << MASTER_ID_POS | slaveId << SLAVE_ID_POS | functionId << FUNCTION_NUMBER_POS | isMasterMessage << 1U),
		data(data, data+DLC) // co jesli data to nullptr, albo dlc jest rowne 0? chbya powinno byc git
{

}

Message::Message(const std::bitset<32>& msgId,
//			unsigned isMasterMessage,
			uint8_t * data,
			unsigned DLC):
					messageId(msgId),
					data(data, data+DLC)
{ }

const std::vector<uint8_t>& Message::getData(void) const
{
	return this->data;
}

//unsigned pow(unsigned a,unsigned b)
//{
//	// działa dla b > 0
//	// to pojebane że musiałem to sam napisać
//	unsigned res = 1;
//	for(;b > 0; --b)
//		res *= a;
//	return res;
//}

// Czy to będzie szybsze, czy obliczy mi wartość w trakcie kompilacji?
template <unsigned a, unsigned b>
unsigned pow()
{
	unsigned res = 1;
	unsigned _b = b;
	for(;_b > 0; --_b)
		res *= a;
	return res;
}

unsigned Message::getMasterId() const
{
//	return (this->messageId >> MASTER_ID_POS).to_ulong() & (pow(2, MASTER_ID_SIZE) - 1);
	return (this->messageId >> MASTER_ID_POS).to_ulong() & (pow<2, MASTER_ID_SIZE>() - 1);
	// JA PIERDOLĘ, W TYM ZJEBANYM JĘZYKU NIE MA STAŁOLICZBOWYCH POTĘG XDDDD
}

unsigned Message::getSlaveId() const
{
//	return (this->messageId >> SLAVE_ID_POS).to_ulong() & (pow(2, SLAVE_ID_SIZE) - 1);
	return (this->messageId >> SLAVE_ID_POS).to_ulong() & (pow<2, SLAVE_ID_SIZE>() - 1);
}

unsigned Message::getFunctionNumber() const
{
//	return (this->messageId >> FUNCTION_NUMBER_POS).to_ulong() & (pow(2, FUNCTION_NUMBER_SIZE) - 1);
	return (this->messageId >> FUNCTION_NUMBER_POS).to_ulong() & (pow<2, FUNCTION_NUMBER_SIZE>() - 1);
}

unsigned Message::getMessageId() const
{
	return this->messageId.to_ulong();
}


} /* namespace RoboCAN */
