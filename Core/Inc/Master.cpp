/*
 * Master.cpp
 *
 *  Created on: Jun 7, 2021
 *      Author: Dell
 */

#include <Master.hpp>

namespace RoboCAN {


Master::Master(unsigned masterId):
		myId(masterId)
{

}

void Master::push_back(Port& newPort)
{
	portVec.push_back(newPort);
}

void Master::transmit(unsigned slaveId, unsigned procedureNr)
{
	auto port = std::find_if(portVec.begin(), portVec.end(),
	                [slaveId](Port& port)
	                {
	                    return port.getId() == slaveId;
	                });
	if(port != portVec.end())
		port->callSlaveFunction(procedureNr, this->myId);
	else
		NoSuchSlaveId_Error();
}

Port::Port(unsigned slaveId, std::function<void(Message&)> onReceive, unsigned masterId):
		onReceive(onReceive),
		slaveId(slaveId)
{
	RoboCAN::filterSettingMaster(masterId, slaveId);
}

unsigned Port::getId()
{
	return this->slaveId;
}

void Port::callSlaveFunction(unsigned functionId, unsigned idMaster)
{
	// Tworzymy ramke z wiadomoscia wymuszajaca funkcje i wysylamy ja
	RoboCAN::sendMessage(Message(idMaster, this->slaveId, functionId, true, nullptr, 0));
}

RoboCAN::Master master(0x1U); // trzeba po prostu zrobic w portach pole statyczne i tyle


} /* namespace RoboCAN */


