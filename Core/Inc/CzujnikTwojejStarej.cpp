/*
 * CzujnikTwojejStarej.cpp
 *
 *  Created on: Jun 7, 2021
 *      Author: Dell
 */

#include <CzujnikTwojejStarej.hpp>


CzujnikTwojejStarej::CzujnikTwojejStarej(int dupa, int id):
	chuj(dupa),
	idTwojejStarej(id)
{
	// Init peryfefriow w cube
	// w sumie to tyle xD
}

void CzujnikTwojejStarej::wlaczDiode(RoboCAN::Message& msg)
{
	LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_5);
}

void CzujnikTwojejStarej::wylaczDiode(RoboCAN::Message& msg)
{
	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_5);
}

void CzujnikTwojejStarej::zapiszDane(RoboCAN::Message& msg)
{
	const int * xd = reinterpret_cast<const int*>(msg.getData().data());
	this->chuj = *xd;
}

int CzujnikTwojejStarej::dajDane(RoboCAN::Message& msg)
{
	RoboCAN::sendData(&(this->chuj), sizeof(this->chuj), msg);
	return this->chuj;
}


