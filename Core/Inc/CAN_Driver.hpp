/*
 * CAN_Driver.hpp
 *
 *  Created on: Jun 7, 2021
 *      Author: Dell
 */

#ifndef INC_CAN_DRIVER_HPP_
#define INC_CAN_DRIVER_HPP_

#include "Message.hpp"
#include "Slave.hpp"
#include "Master.hpp"
#include "can.h"
#include "stm32f1xx.h"
#include <bitset>
#include <algorithm>



namespace RoboCAN {



Message getMessage(unsigned whichFifo);

void filterSettingSlave(unsigned myId);

void filterSettingMaster(unsigned masterId, unsigned slaveId);

// inicjalizacja
void CAN_init();


void sendMessage(const Message& msg); // zmienic voida

//const Message& receiveMessage();

void sendData(void * data, unsigned size, const Message& msg);


// Pierwotnie procedury beda wywolywane wprost z przerwania, potem zmieni sie to na wrzucanie wiadomosci do kolejki
// A nastepnie wiadomosci te beda odczytywane w petli glownej i wykonywane.

// Przerwanie od fifo0
void USB_LP_CAN1_RX0_IRQHandler();

//PRzerwanie od fifo1
void CAN1_RX1_IRQHandler();

// prezrwanie od wysylania
void USB_HP_CAN1_TX_IRQHandler();

// a to jest przerwanie od chuj wie czego
void CAN1_SCE_IRQHandler();

// Tutaj można powrzucać funkcje wznoszące jakieś errory, w dalszej kolejności można użyć wyjątków
void NoSuchSlaveId_Error();


}
extern "C" void USB_LP_CAN1_RX0_IRQHandler();
extern "C" void CAN1_RX1_IRQHandler();




#endif /* INC_CAN_DRIVER_HPP_ */
