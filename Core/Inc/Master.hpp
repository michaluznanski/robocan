/*
 * Master.hpp
 *
 *  Created on: Jun 7, 2021
 *      Author: Dell
 */

#ifndef INC_MASTER_HPP_
#define INC_MASTER_HPP_

#include <vector>
#include <functional>
#include <algorithm>

#include "Message.hpp"
#include "CAN_Driver.hpp"

namespace RoboCAN {

class Port
{
public:
    Port(unsigned slaveId, std::function<void(Message&)> onReceive, unsigned masterId);

    unsigned getId();
    std::function<void(Message&)> onReceive;

    void callSlaveFunction(unsigned functionId, unsigned masterId);
private:
    unsigned slaveId;

    //np. onReceive = [global_variable](Message& message) { global_variable = message.getData();}
};



class Master {
public:
    Master(unsigned masterId);
    void push_back(Port& newPort); //{ transceiverVec.push_back(newTr); }
    void transmit(unsigned slaveId, unsigned procedureNr);
    std::vector<Port>::iterator begin() {return portVec.begin(); }
    std::vector<Port>::iterator end() {return portVec.end(); }
    inline unsigned getMasterId()const {return this->myId;}

private:
    std::vector<Port> portVec;
    const unsigned myId;
};

extern Master master;

} /* namespace RoboCAN */

#endif /* INC_MASTER_HPP_ */
